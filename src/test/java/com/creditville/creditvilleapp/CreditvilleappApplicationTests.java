package com.creditville.creditvilleapp;

import com.creditville.creditvilleapp.configuration.UrlParameter;
import com.creditville.creditvilleapp.dto.ClientDTO;
import com.creditville.creditvilleapp.dto.EmployeeDTO;
import com.creditville.creditvilleapp.models.Employee;
import com.creditville.creditvilleapp.services.EmployeeService;
import com.creditville.creditvilleapp.services.implementation.ClientImp;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;
import java.util.List;

@SpringBootTest
class CreditvilleappApplicationTests {

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private ClientImp clientImp;

    @Autowired
    private UrlParameter urlParameter;

    @Value("${ThirdpartyConfig.CbaBaseUrl}")
    private String baseUrl;
    @Value("${ThirdpartyConfig.withdrawal}")
    private String withdrawsalUrl;

    @Value("${ThirdpartyConfig.LookUpClientEndpoint}")
    private String getClientUrl;

    @Value("${ThirdpartyConfig.LookUpClientEndpoint}")
    private String getClientByBvnUrl;

    @Test
    void contextLoads() {
    }

    @Test
    void CreateEmployee(){
        EmployeeDTO employeeDTO = new EmployeeDTO();
        employeeDTO.setFirstName("Emmanuel");
        employeeDTO.setLastName("Japhet");
        employeeDTO.setEmail("emmanuel@gmail.com");
        employeeDTO.setPhoneNumber("07050557534");
        employeeDTO.setDateOfBirth("1990-10-10");
        employeeDTO.setDepartment("Info Tech");
        employeeDTO.setEmploymentDate(new Date());

        Employee employee = employeeService.CreateEmployee(employeeDTO);
        System.out.println("employee created successfully: " + employee);
    }


    @Test
    void getEmployees(){
        List<Employee> getEmployees = employeeService.getEmployees();
        System.out.println(getEmployees);
    }

    @Test
    void getById(){
//        Employee getByIdtEmployee = employeeService.getById(1);
//        System.out.println(getEmployee);
        ClientDTO clientDTO = new ClientDTO();
        clientDTO.setDob("");

        var response = clientImp.CreateClient(clientDTO);
    }



    @Test
    void getEmployee(){
        Employee get = employeeService.getEmployee("emmanuel@gmail.com");
        System.out.println(get);
    }


    @Test
    void getClientByBvn(){
        var url = baseUrl + getClientByBvnUrl+"/";
        var resp = urlParameter.getHttpCall(url,"22535301194");
        System.out.println("resp: "+resp);
    }

    @Test
    void getClient(){
        var url = baseUrl + getClientUrl+"/";
        var resp = urlParameter.getHttpCall(url,"22535301194");
        System.out.println("resp: "+resp);
    }

}
