package com.creditville.creditvilleapp.controller;

import com.creditville.creditvilleapp.dto.EmployeeDTO;
import com.creditville.creditvilleapp.models.Client;
import com.creditville.creditvilleapp.models.Employee;
import com.creditville.creditvilleapp.services.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = {"/employee"})
public class EmployeeController {
    @Autowired
    private EmployeeService employeeService;

    @PostMapping(value = {"/create"}, produces = "application/json")
    public ResponseEntity<Object> createEmployee(@RequestBody EmployeeDTO employeeDTO) {
        System.out.println("employeeDTO: " + employeeDTO.toString());
        Employee employee = employeeService.CreateEmployee(employeeDTO);
        //employee.setEmploymentDate(new Date());
        System.out.println("employee: " + employee);
        return ResponseEntity.ok(employee);
    }


    @GetMapping(value = {"/getEmployees"}, produces = "application/json")
    public List<Employee> getEmployees(){
        List<Employee> employees = employeeService.getEmployees();
        System.out.println(employees);

        return employees;
    }

    @GetMapping(value = {"/getEmployee/{id}"}, produces = "application/json")
    public Employee getEmployeeById(@PathVariable long id) {
        return employeeService.getById(id);
    }

    @GetMapping(value = {"/getclient/{id}"}, produces = "application/json")
    public String getClient(@PathVariable String id) {
        var clientResp = employeeService.getClient(id);
        
        return clientResp;
    }

}
