package com.creditville.creditvilleapp.services.implementation;

import com.creditville.creditvilleapp.configuration.ThirdpartyConfig;
import com.creditville.creditvilleapp.configuration.UrlParameter;
import com.creditville.creditvilleapp.dto.ClientDTO;
import com.creditville.creditvilleapp.models.Client;
import com.creditville.creditvilleapp.repository.ClientRepository;
import com.creditville.creditvilleapp.services.ClientService;
import org.json.JSONObject;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClientImp implements ClientService {
    @Autowired
    ClientRepository clientRepository;

    @Autowired
    ModelMapper modelMapper;

    @Autowired
    ThirdpartyConfig config;

    @Autowired
    private UrlParameter urlParameter;

    @Value("${ThirdpartyConfig.CbaBaseUrl}")
    private String baseUrl;

    @Value("${ThirdpartyConfig.withdrawal}")
    private String withdrawsalUrl;

    @Value("${ThirdpartyConfig.LookUpClientEndpoint}")
    private String getClientUrl;

    @Value("${ThirdpartyConfig.LookUpClientEndpoint}")
    private String getClientByBvnUrl;



    @Override
    public String CreateClient(ClientDTO clientDTO) {
        Client client = new Client();
        JSONObject object = new JSONObject();
        object.put("account_id","");
        object.put("payment_method_name","");
        object.put("transaction_date","");
        object.put("transaction_branch_id","");
        object.put("amount","");
        object.put("notes","");

        var withdrwResp = urlParameter.postHttpCall(object.toString());
        System.out.println("withdrwResp: "+withdrwResp);


        return withdrwResp;
    }

    @Override
    public Client updateClient(ClientDTO clientDTO) {
        return null;
    }

    @Override
    public Client getClient(String email) {
        return null;
    }

    @Override
    public String getClientByBvn(String bvn) {
        var clientUrl = baseUrl + getClientByBvnUrl + "/";
        var clientResponse = urlParameter.getHttpCall(clientUrl, bvn);
        System.out.println("clientResponse: "+clientResponse);
        return clientResponse;
    }

    @Override
    public List<Client> getClients() {
        return null;
    }

    @Override
    public Client delete(String email) {
        return null;
    }

    @Override
    public String deleteClient(String bvn) {
        return null;
    }



    //Manual Mapping
    private Client convertClientDtoToCliententity(ClientDTO clientDTO){
        Client client = new Client();
        client.setFirst_name(clientDTO.getFirst_name());
        client.setMiddle_name(clientDTO.getMiddle_name());
        client.setLast_name(clientDTO.getLast_name());
        client.setBvn(clientDTO.getBvn());
        client.setPhone_number(clientDTO.getPhone_number());
        client.setEmail(clientDTO.getEmail());
        client.setDob(clientDTO.getDob());
        client.setAddress(clientDTO.getAddress());
        client.setCity(clientDTO.getCity());
        client.setCountry(clientDTO.getCountry());
        client.setState(clientDTO.getState());
        client.setPhoto_url(clientDTO.getPhoto_url());
        client.setStreet(clientDTO.getStreet());
        client.setAccount_name(clientDTO.getAccount_name());
        return client;
    }

    //Auto Mapping
    private Client convertDtoToEntity(ClientDTO clientDTO){
        return modelMapper.map(clientDTO, Client.class);
    }

}
