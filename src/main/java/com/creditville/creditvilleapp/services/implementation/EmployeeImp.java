package com.creditville.creditvilleapp.services.implementation;

import com.creditville.creditvilleapp.configuration.UrlParameter;
import com.creditville.creditvilleapp.dto.EmployeeDTO;
import com.creditville.creditvilleapp.exception.AppException;
import com.creditville.creditvilleapp.models.Employee;
import com.creditville.creditvilleapp.repository.EmployeeRepository;
import com.creditville.creditvilleapp.services.EmployeeService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class EmployeeImp implements EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private UrlParameter urlParameter;

    @Value("${ThirdpartyConfig.CbaBaseUrl}")
    private String baseUrl;
    @Value("${ThirdpartyConfig.withdrawal}")
    private String withdrawsalUrl;

    @Value("${ThirdpartyConfig.LookUpClientEndpoint}")
    private String getClientUrl;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public Employee CreateEmployee(EmployeeDTO employeeDTO) {
        Employee employee = convertEmployeeDtoToEmployeeEntity(employeeDTO);
        System.out.println("employee " + employee);
        Employee createEmployee = employeeRepository.findEmployeeByEmail(employeeDTO.getEmail());
        if(createEmployee != null){
            throw new AppException.EmAuthException("Email Already Exit");
        }
        return employeeRepository.save(employee);
    }

    @Override
    public Employee updateEmployee(EmployeeDTO employeeDTO) {
        Employee employee = convertEmployeeDtoToEmployeeEntity(employeeDTO);

        return null;
    }

    @Override
    public Employee getEmployee(String email) {
        return employeeRepository.findEmployeeByEmail(email);
    }

    @Override
    public List<Employee> getEmployees() {
        return employeeRepository.findAll();
    }

    @Override
    public Employee delete(String email) {
        return null;
    }

    @Override
    public String deleteEmployee(long id) {
        return null;
    }

    @Override
    public Employee getById(long id) {
        return employeeRepository.findById(id).orElse(null);
    }

    @Override
    public String getClient(String id) {
        var clientUrl = baseUrl + getClientUrl + "/";
        var clientResp = urlParameter.getHttpCall(clientUrl, id);
        System.out.println("clientResp: "+clientResp);
        return clientResp;
    }

    //NOTE: YOU MUST CONVERT EMPLOYEEDTO TO EMPLOYEE ENTITY TO BE ABLE TO ACCEPT THE DETAIL FROM THE USER WITHOUT ERROS
    // TO DO THIS YOU MUST MAP THE DTO TO THE ENTITY USING MODELMAPPER AUTOMATICALLY OR MANUALLY MAP THEM. EXAMPLE BELOW

    private Employee convertEmployeeDtoToEmployeeEntity(EmployeeDTO employeeDTO){
        Employee employee = new Employee();
        employee.setFirstName(employeeDTO.getFirstName());
        employee.setLastName(employeeDTO.getLastName());
        employee.setEmail(employeeDTO.getEmail());
        employee.setPhoneNumber(employeeDTO.getPhoneNumber());
        employee.setDateOfBirth(employeeDTO.getDateOfBirth());
        employee.setDepartment(employeeDTO.getDepartment());
        employee.setEmploymentDate(new Date());

        return employee;
    }

    private Employee convertDtoToEntity(EmployeeDTO employeeDTO){
        return modelMapper.map(employeeDTO, Employee.class);
    }
}
