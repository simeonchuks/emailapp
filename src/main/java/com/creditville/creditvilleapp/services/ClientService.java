package com.creditville.creditvilleapp.services;

import com.creditville.creditvilleapp.dto.ClientDTO;
import com.creditville.creditvilleapp.models.Client;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ClientService {
    String CreateClient(ClientDTO clientDTO);

    Client updateClient(ClientDTO clientDTO);

    Client getClient(String email);

   // Client getById(long id);

    String getClientByBvn(String bvn);

    List<Client> getClients();

    Client delete(String email);

    String deleteClient(String bvn);



}
