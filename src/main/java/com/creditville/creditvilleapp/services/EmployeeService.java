package com.creditville.creditvilleapp.services;

import com.creditville.creditvilleapp.dto.EmployeeDTO;
import com.creditville.creditvilleapp.models.Employee;

import java.util.List;

public interface EmployeeService {
    Employee CreateEmployee(EmployeeDTO employeeDTO);

    Employee updateEmployee(EmployeeDTO employeeDTO);

    Employee getEmployee(String email);

    List<Employee> getEmployees();

    Employee delete(String email);

    String deleteEmployee(long id);

    Employee getById(long id);

    String getClient(String id);

}
