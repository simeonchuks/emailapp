package com.creditville.creditvilleapp.configuration;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class UrlParameter {


    @Value("${ThirdpartyConfig.CbaBaseUrl}")
    private String baseUrl;
    @Value("${ThirdpartyConfig.withdrawal}")
    private String withdrawsalUrl;

    @Value("${ThirdpartyConfig.LookUpClientEndpoint}")
    private String getClientUrl;

    public String postHttpCall(String payload){
        Unirest.setTimeouts(0, 0);
        HttpResponse<String> response= null;
        try {
            response = Unirest.post(baseUrl+withdrawsalUrl)
                    .header("apiKey", "7oYOYD+eXgltrbUbS0ytUz+7v1d7ton05ZdAhqVJ/vw=")
                    .header("Content-Type", "application/json")
                    .body(payload)
                    .asString();
        } catch (UnirestException e) {
            e.printStackTrace();
        }
        return response.getBody();
    }

    public String getHttpCall(String url , String acct){
        Unirest.setTimeouts(0, 0);
        HttpResponse<String> response= null;
        try {
            response = Unirest.get(url+acct)
                    .header("apiKey", "7oYOYD+eXgltrbUbS0ytUz+7v1d7ton05ZdAhqVJ/vw=")
                    .header("Content-Type", "application/json")
                    .asString();
        } catch (UnirestException e) {
            e.printStackTrace();
        }
        return response.getBody();
    }



}
