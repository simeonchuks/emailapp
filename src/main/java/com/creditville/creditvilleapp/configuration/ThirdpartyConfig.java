package com.creditville.creditvilleapp.configuration;

import org.springframework.stereotype.Service;

@Service
public class ThirdpartyConfig {

    private String CbaBaseUrl;
    private String CreateClientEndpoint;
    private String LookUpClientEndpoint;
    private String apiKey;

    public String getCbaBaseUrl() {
        return CbaBaseUrl;
    }

    public void setCbaBaseUrl(String cbaBaseUrl) {
        CbaBaseUrl = cbaBaseUrl;
    }

    public String getCreateClientEndpoint() {
        return CreateClientEndpoint;
    }

    public void setCreateClientEndpoint(String createClientEndpoint) {
        CreateClientEndpoint = createClientEndpoint;
    }

    public String getLookUpClientEndpoint() {
        return LookUpClientEndpoint;
    }

    public void setLookUpClientEndpoint(String lookUpClientEndpoint) {
        LookUpClientEndpoint = lookUpClientEndpoint;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }


/*public class ThirdPartyConfig
    {
        public string CbaBaseUrl { get; set; }
        public string CreateClientEndpoint { get; set; }
        public string LookUpClientEndpoint { get; set; }
        public string CreateLoanEndpoint { get; set; }
        public string LookUpLoanEndpoint { get; set; }
        public string CreateDepositEndpoint { get; set; }
        public string LookUpDepositEndpoint { get; set; }
        public string apiusername { get; set; }
        public string apipassword { get; set; }

    }*/

}
