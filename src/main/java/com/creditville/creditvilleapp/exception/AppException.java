package com.creditville.creditvilleapp.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

public class AppException {
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public static class EmAuthException extends RuntimeException{
        public EmAuthException(String message) { super(message);}
    }
}
